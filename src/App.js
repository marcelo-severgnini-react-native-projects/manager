import React, { Component} from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import LoginForm from './components/LoginForm';
import Router from './Router';


class App extends Component {

    componentWillMount(){
        var config = {
            apiKey: 'AIzaSyD2M74yZz_SyUAlhw0c1Y16LWAWdyFi2uA',
            authDomain: 'manager-2ac35.firebaseapp.com',
            databaseURL: 'https://manager-2ac35.firebaseio.com',
            projectId: 'manager-2ac35',
            storageBucket: '',
            messagingSenderId: '490359428835'
        };
        firebase.initializeApp(config);
    }

    render(){
        
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk)); 

        return (

            <Provider store={store}>
                <Router />
            </Provider>
            
        );
    };
}

export default App;